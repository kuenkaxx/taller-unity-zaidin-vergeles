﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class KillPlayer : MonoBehaviour
{
    AIPath aiPath;
    AudioSource AS;
    public AudioClip die;
    GameController GC;

    private void Start()
    {
        aiPath = FindObjectOfType<AIPath>();
        AS = GetComponent<AudioSource>();
        GC = FindObjectOfType<GameController>();
    }

    IEnumerator Matar(GameObject Player) {
        print("MATO");
        Destroy(aiPath);
        Destroy(Player.GetComponent<PlayerController>());
        Destroy(Player.GetComponent<CharacterController>());
        yield return new WaitForEndOfFrame();
        foreach (Collider2D col in Player.GetComponents<Collider2D>())
        {
            Destroy(col);
        }
        yield return new WaitForEndOfFrame();
        Rigidbody2D rb = Player.GetComponent<Rigidbody2D>();
        rb.AddForce(transform.up * 20, ForceMode2D.Impulse);
        yield return new WaitForEndOfFrame();

        Animator anim = Player.GetComponent<Animator>();
        anim.SetTrigger("hurt");
        AS.PlayOneShot(die);
        yield return new WaitForSeconds(die.length);
        GC.Morir();
        yield return new WaitForEndOfFrame();

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            StartCoroutine(Matar(collision.gameObject));
        }
    }
}
