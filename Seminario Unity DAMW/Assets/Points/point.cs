﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class point : MonoBehaviour
{
    public AudioClip GetSound;
    public Animator anim;
    public GameController GC;

    // Start is called before the first frame update
    void Start()
    {
        GC = FindObjectOfType<GameController>();
    }

    void GetPoint() {
        StartCoroutine("SetPoint");
    }

    IEnumerator SetPoint()
    {
        Destroy(this.GetComponent<Collider2D>());
        yield return new WaitForEndOfFrame();

        GameObject Sound = new GameObject();
        Instantiate(Sound, this.transform.position, this.transform.rotation);
        Sound.AddComponent<AudioSource>();
        Sound.GetComponent<AudioSource>().PlayOneShot(GetSound);
        anim.SetTrigger("pum");
        yield return new WaitForSeconds(GetSound.length + 1);
        Destroy(Sound.gameObject);
        yield return new WaitForEndOfFrame();
    }

    public void AddPoint() {
        if (GC != null)
        {
            GC.AddPoint(); 
        }
    }

    public void Matame()
    {
        Destroy(this.gameObject);

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            GetPoint();
        }
    }
}
