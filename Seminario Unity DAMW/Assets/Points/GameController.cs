﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Text textPoint;
    public Animator animMorir;

    [SerializeField]
    int maxPoints;

    [SerializeField]
    int actualPoints;
    // Start is called before the first frame update
    void Start()
    {
        actualPoints = 0;
        ActualizarTextos();
    }

    public void AddPoint() {
        actualPoints = actualPoints + 1;
        ActualizarTextos();
        didIWon();
    }

    void ActualizarTextos() {
        textPoint.text = (actualPoints + "/" + maxPoints);  

    }

    void didIWon(){
        if (actualPoints == maxPoints)
        {
            print("He Ganado");
        }

    }

    public void Morir() {
        animMorir.SetTrigger("lose");
    }

}
